import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

export default function Modal({ children, onDelete }) {
  const[show, setShow] = useState(false);

  const onDeleteHandler = ()=>{
    onDelete();
    setShow(!show);
  };

  return (
    <>
      <FontAwesomeIcon icon={faTrash} onClick={()=>setShow(!show)}/>
      { show &&
        <div className="modal absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center bg-black/[.6]" onClick={()=>setShow(!show)}>
          <div className="modal-content w-96 bg-white rounded-lg">
            <div className="modal-header p-3">
              <h4 className="m-0 text-black text-center text-2xl font-bold">
                Delete?
              </h4>
            </div>
            <div className="modal-body p-5 text-black text-left">{children}</div>
            <div className="moda-footer p-3 text-black text-right">
              <button
                className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
                onClick={()=>setShow(!show)}
              >
                Close
              </button>
              <button
                className="bg-red-600 text-white font-semibold py-2 px-4 hover:bg-red-900 rounded border border-red-600 hover:border-red-900  ml-1"
                onClick={onDeleteHandler}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      }
    </>
  );
}

Modal.propTypes = {
  children: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
};
