import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Button.css';


export default function Button({ children, dest, style, type }) {

  return (
    dest !== null
    ? <Link to={dest}>
        <button className={style} type={type}>
          {children}
        </button>
    </Link>
    : <button className={style} type={type}>
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  dest: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
