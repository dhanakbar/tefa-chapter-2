/* eslint-disable import/extensions */
import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import ShallowRenderer from 'react-test-renderer/shallow';
import data from '../product.json';
import Products from '../Products';



jest.mock('react-router-dom', ()=> ({
  Link: () => {
    const link = props => <link {...props} />;
    return link;
  },
  useLocation: jest.fn(() => ({
    state: null
  }))
}));

//mock element
jest.mock('../../../components/elements/Table/Table', () => {
  const table = props => <table {...props} />;
  return table;
});
jest.mock('../../../components/elements/Modal', () => {
  const modal = props => <modal {...props} />;
  return modal;
});
jest.mock('../../../components/elements/Button', () => {
  const button = props => <button {...props} />;
  return button;
});
jest.mock('../../../components/elements/Select/Select', () => {
  const select = props => <select {...props} />;
  return select;
});
jest.mock('../../../components/elements/InputField/InputFIeld', () => {
  const inputField = props => <input {...props} />;
  return inputField;
});



describe('src/pages/Products', () => {

  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Products />);
    expect(tree).toMatchSnapshot();
  });

  test('setItemsData when useLocation state not null', () => {

    const dummy = {
      'id': 'sku666',
      'name': 'test1',
      'price': 666,
      'image': 'test.jpg',
      'isDeleted': false,
      'category': 'Pre-Order',
      'expiryDate': null
    };
    useLocation.mockImplementationOnce(() => ({
      state: dummy
    }));


    // function jest.fn bisa dipakai untuk expect()
    // supaya bisa masuk kedalam kode, jest.fn disisipin ke dalam useState
    // sebutan lainnya spyOn https://jestjs.io/docs/jest-object#jestspyonobject-methodname

    const setItemsData = jest.fn();
    useState.mockImplementationOnce(value => [value, setItemsData]);
    Products();//eksekusi (invoke)
    expect(setItemsData).toHaveBeenCalledWith([dummy, ...data]);
  });

  test('render rowData with opacity', () => {
    //invoke
    const res = Products();
    const table = res.props.children[2];
    // console.log(table);

    //expect
    const withOpacity = table.props.rows(data[0], 0);
    expect(withOpacity.props.children.props.className).toBe('p-2 opacity-40');

    // satu test case bisa lebih dari satu expect untuk menghemat proses invoke
    const wihtoutOpacity = table.props.rows(data[1], 1);
    expect(wihtoutOpacity.props.children.props.className.trim()).toBe('p-2');
  });

  test('onDelete', () => {
    //setItemsData untuk di expect
    const setItemsData = jest.fn();
    useState.mockImplementationOnce(value => [value, setItemsData]);

    //invoke
    const res = Products();
    const table = res.props.children[2];
    const row = table.props.rows(data[0], 0);
    const modal = row.props.children.props.children[4].props.children;
    // console.log(table);

    //invoke onDelete
    // expect, karena bentuknya array of object jadi perlu dibongkar ke dalam pakai arrayContaining dan objectContaining
    modal.props.onDelete();
    expect(setItemsData).toHaveBeenCalledWith(
      expect.arrayContaining([
        expect.objectContaining({ id: data[0].id, isDeleted: true })
      ])
    );
  });
});
